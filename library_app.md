
# tutorial baseado em https://frappeframework.staging.frappe.cloud/docs/user/en/tutorial

Após o frappe framework instalado, vá para o diretório

    cd frappe-bench

crie um novo app

    bench new-app library_management

crie um novo site

    bench new-site library.local

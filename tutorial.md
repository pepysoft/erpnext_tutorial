# Tutorial ErpNext

## instalação wsl custom folder

https://damsteen.nl/blog/2018/08/29/installing-wsl-manually-on-non-system-drive

https://aka.ms/wsl-ubuntu-1804

Usando powershell

    Invoke-WebRequest -Uri https://aka.ms/wsl-ubuntu-1804 -OutFile ErpNext.appx -UseBasicParsing

Extraia

    Rename-Item .\ErpNext.appx ErpNext.zip
    Expand-Archive .\ErpNext.zip -Verbose


In the extracted directory you will find an .exe file that allows installing your linux distribution. In my case, it was ubuntu1804.exe. Simply run it in your current command line shell and installation will start.



## Instalação Manual

https://github.com/frappe/frappe/wiki/The-Hitchhiker%27s-Guide-to-Installing-Frappe-on-Linux

    sudo apt-get install git
    git --version
    sudo apt-get install python3-dev
    sudo apt-get install python3-setuptools python3-pip
    sudo apt-get install virtualenv

    sudo apt-get install software-properties-common
    sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
    sudo add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://ftp.ubuntu-tw.org/mirror/mariadb/repo/10.3/ubuntu xenial main'

    sudo apt-get update
    sudo apt-get install mariadb-server-10.3

    sudo apt-get install libmysqlclient-dev



$ sudo mysql_secure_installation

Remember: only run sudo mysql_secure_installation if you're not prompted the password during setup.

    sudo nano /etc/mysql/my.cnf

    [mysqld]
    character-set-client-handshake = FALSE
    character-set-server = utf8mb4
    collation-server = utf8mb4_unicode_ci

    [mysql]
    default-character-set = utf8mb4


    sudo service mysql restart

    sudo apt-get install redis-server

    sudo apt-get install curl
    curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
    sudo apt-get install -y nodejs    

    sudo npm install -g yarn


## video 1
https://www.youtube.com/watch?v=eCAMPcl7NKc&list=PL3lFfCEoMxvzHtsZHFJ4T3n5yMM3nGJ1W

1) criar um novo app, dentro de frappe-bench
    bench new-app nomedoapp

2) arquivo hooks.py define a configuração básica do app e como o app connecta com outros apps

    nomedoapp/nomedoapp/hooks.py

3) crie um novo site e installe o app naquele site

    bench new-site meeting.dev
    bench --site meeting.dev install-app meeting
    bench --site meeting.dev set-config developer_mode 1

4) adicione host_name in site_config.json

    {
        "db_name": "_bcad64afbf268a6a",
        "db_password": "phVJ0MiqX3gjZWBw",
        "db_type": "mariadb",
        "developer_mode": 1,
        "host_name":"http://meeting.local",
    }

5) bench start

ps: se tiver um erro de address already in use:

    sudo supervisorctl stop all

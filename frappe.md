# Frappe tutorial

https://frappeframework.com/

## prepare wsl ubuntu 20

Instale Ubuntu/Ubuntu 20.04


## opcional - exporte e importe com outro nome
Lista as vms 

    wsl -l

Exporta a vm 



    wsl.exe --export Ubuntu frappe.tar 


importe como frappe

    wsl --import frappe d:\wsl\frappe d:\wsl\frappe.tar

veja que há uma nova distro

    wsl -l

inicie a distro escolhida

    wsl -d frappe


## vscode 

Configurando o vscode para wsl

a) instale o plugin Remote-wsl 

b) aperte F1 e digite/escolha 'Remote-wsl: New Windows using distro'. escolha 'frappe'.



## instalando frappe developer mode

https://frappeframework.com/docs/user/en/installation



    sudo apt update
    sudo apt dist-upgrade
    sudo apt install git python3-dev redis-server python3-pip
    sudo apt-get install software-properties-common
    sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
    sudo add-apt-repository 'deb [arch=amd64, ppc64el] http://ftp.ubuntu-tw.org/mirror/mariadb/repo/10.3/ubuntu focal main'
    sudo apt-get update
    sudo apt-get install mariadb-server-10.3


Edite a configuração do mariadb 

    sudo nano /etc/mysql/mariadb.conf.d/50-server.cnf

adicionando esse conteúdo nas seções correspondentes


    [mysqld]
    character-set-client-handshake = FALSE
    character-set-server = utf8mb4
    collation-server = utf8mb4_unicode_ci

observe as duas ultimas linhas já tem configuração. comente ou altere

e no arquivo 
    sudo nano /etc/mysql/conf.d/mysql.cnf

inclua

    [mysql]
    default-character-set = utf8mb4


Reinicie o mysql

    sudo service mysql restart


se não for pedido a senha de root na instalação

    sudo mysql_secure_installation

se esquecer a senha de root do mariadb

    sudo service mysql stop
    sudo mysqld_safe --skip-grant-tables &
    mysql -u root
    
use os comandos para alterar a senha:

    USE mysql;
    UPDATE user SET plugin='mysql_native_password' WHERE User='root';
    UPDATE mysql.user SET authentication_string = PASSWORD('x') WHERE User = 'root' AND Host = 'localhost';
    flush privileges;
    exit

reinicie o mariadb:

    sudo service mysql stop
    sudo service mysql start



## Install node

O modo descrito no site não funciona, então:

    sudo apt install nodejs npm


Teste o node

    node -v

Instale o yarn

    sudo npm install -g yarn

## instale o bench

    git clone https://github.com/frappe/bench .bench
    pip3 install testresources
    

    
adicione a pasta .local/bin ao path, adicionando a linha abaixo no final do .bashrc

    export PATH="/home/x/.local/bin:$PATH"

e também execute no terminal para usar na mesma sessão

    export PATH="/home/x/.local/bin:$PATH"

finalmente instale o bench para o usuário

    pip3 install --user -e .bench



## tutorial baseado em https://frappeframework.com/docs/user/en/tutorial


### criando um bench para o tutorial

Um 'bench' pode ser considerado 'um ambiente de desenvolvimento' ou eventualmente 'um ambiente de produção'


Podemos criar pastas distintas que representem ambientes de produção diferentes,  Vamos criar uma pasta 'bench'  para o nosso projeto. 

    bench init tutorial-frappe-bench

A criação do diretorio/ambiente de desenvolvimento irá demorar um pouco. Depois que terminar vá para o diretório criado. 

    cd tutorial-frappe-bench



Por mais que os tutoriais mencionem iniciar o bench ('bench start') ou criar um 'app' primeiro, isso não faz sentido. O servidor de desenvolvimento só funcionará corretamente quando criar um site, e no ambiente de desenvolvimento recomendo criar o localhost

    bench new-site localhost

Ao criar o site, será solicitada a senha do root do mysql. Ele criará um banco de dados, usuário e senha para o site.
Também será solicitada uma senha para o usuário Administrator do site

Inicie o servidor de desenvolvimento:

    bench start

esse terminal ficará ocupado com o stdout. Abra um novo terminal para continuar trabalhando.

vá para http://localhost:8000 e faça login como Administrator e a senha informada na criação do site. 

Escolha a linguagem do site, país, fuso horário e moeda. Para efeito deste tutorial, vamos manter a lingua do site em inglês, tendo vista que os clientes são estrangeiros, a moeda dolar e o fuso horário América/New_York

A seguir, cadastre o seu primeiro usuário, você mesmo

Abra outro terminal (se não abriu antes) e execute o comando para ativar o modo desenvolvedor:

    bench --site localhost set-config developer_mode 1

O arquivo sites/meeting.local/site_config.json para ativar o 'developer_mode' 


{
 "db_name": "_ac155bbff90760dd",
 "db_password": "zhwMFp90mWfvqZ5N",
 "db_type": "mariadb",
 "developer_mode":1
}


Pare o servidor de desenvolvimento no terminal com Crtl+c e inicie novamente

    bench start



### Criando um app

No outro terminal, no diretorio tutorial-frappe-bench execute
    
    bench new-app library


Complete as informações solicitadas

Leia a pagina https://frappeframework.com/docs/user/en/tutorial/new-app para entender a estrutura de um novo app


Para instalar um app no site use o comando abaixo:

    bench --site localhost install-app library


novamente use Crtl+c para sair do servidor de desenvolvimento e reinicie

    bench start



## criando DocTypes

DocTypes são basicamente 'models' em uma estrutura MVC. Quando se cria um DocType, se cria também uma tabela no banco de dados e uma view. 



